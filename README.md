# Same Height

Align HTML elements together, based on the tallest.

**No dependencies**. Fully responsive. Unlimited selectors.

## How to use

```twig
{{ attach_library('same_height/same_height') }}
{% set sameHeight = ['.item-title', '.item-description']|json_encode %}
<div{{ attributes.addClass('row').setAttribute('data-same-height',  sameHeight) }}>
  {% for item in items %}
    <div class="col-12 col-md-4 item">
      <h3 class="item-title">{{ item.title }}</h3>
      <p class="item-description">{{ item.description}}</p>
    </div>
  {% endfor %}
</div>
```

```html
<!-- Align titles -->
<div class="row items" data-same-height="[&quot;.item-title&quot;]">
    <div class="col-12 col-md-4 item">
      <h3 class="item-title">This is a title</h3>
      <p class="item-description">This is an interesting description.</p>
      <a href="#">Read more</a>
    </div>
    <div class="col-12 col-md-4 item">
      <h3 class="item-title">This is a very very very long title</h3>
      <p class="item-description">This is an interesting description.</p>
      <a href="#">Read more</a>
    </div>
    <div class="col-12 col-md-4 item">
      <h3 class="item-title">This is a title</h3>
      <p class="item-description">This is an interesting description.</p>
      <a href="#">Read more</a>
    </div>
    <div class="col-12 col-md-4 item">
      <h3 class="item-title">This is a title</h3>
      <p class="item-description">This is an interesting description.</p>
      <a href="#">Read more</a>
    </div>
    <div class="col-12 col-md-4 item">
      <h3 class="item-title">This is a title</h3>
      <p class="item-description">This is an interesting description.</p>
      <a href="#">Read more</a>
    </div>
</div>
```

## Credits

<a href="https://www.flaticon.com/free-icons/height" title="height">Height icon created by Freepik - Flaticon</a>
