/**
 * @file
 * Defines a custom behavior to fake alignment of any HTML elements.
 */
(function (Drupal, debounce, once) {
  const onceName = 'sameHeight';

  // Attribute where you can tell which elements must be align.
  // Elements with the data-same-height attribute starting "[" and ending in "]".
  // Because we expects the value to be a JSON-encoded array, such as:
  // <div data-same-height="[&quot;.lead&quot;]"><!-- content --></div>
  const wrapper = '[data-same-height^="["][data-same-height$="]"]';

  /**
   * Alter sibling elements to fake vertical alignment.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior to the rendering context, if possible.
   */
  Drupal.behaviors.sameHeight = {
    attach(context) {
      const onResize = (e) => {
        document.querySelectorAll(wrapper).forEach((element) => {
          JSON.parse(element.dataset.sameHeight).forEach((selector) => {
            resetHeight(element, selector);
            sameHeight(element, selector);
          });
        });
      }

      // Reset original element's height.
      const resetHeight = (element, selector) => {
        element.querySelectorAll(selector).forEach((child) => {
          child.style.height = null;
        });
      }

      // Align siblings with the tallest element.
      const sameHeight = (element, selector) => {
        let values = {};
        // Prepare list of heights of sibling elements position.
        // Siblings elements are grouped together based on their top position.
        // We use rect.top to identify elements on different "rows".
        element.querySelectorAll(selector).forEach((child) => {
          let position = child.getBoundingClientRect();
          let y = Math.ceil(position.top);
          child.dataset.sameHeightTop = y;
          if (typeof values[y] == 'undefined') {
            values[y] = [];
          }
          values[y].push(position.height);
        });

        // Apply tallest now.
        element.querySelectorAll(selector).forEach((child) => {
          let y = child.dataset.sameHeightTop;
          let tallest = Math.max(...(values[y]));
          child.style.height = tallest + 'px';
        });
      }

      // Calculate same height on window load.
      once(onceName, wrapper, context).forEach((element) => {
        try {
          // Get target selectors from JSON-encoded list of valid DOM selectors.
          JSON.parse(element.dataset.sameHeight).forEach((selector) => {
            sameHeight(element, selector);
          });
        } catch (e) {
          // Fails but do not block the rest of the page.
          console.error(Drupal.t('Error in sameheight.js'), e);
        };
      });

      // Calculate same height on window resize.
      window.addEventListener('resize', debounce(onResize, 250, false));
    },
    detach(context, settings, trigger) {
      if (trigger === 'unload') {
        once.remove(onceName, wrapper, context);
      }
    },
  };
})(Drupal, Drupal.debounce, once);
